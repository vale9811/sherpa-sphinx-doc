Sherpa |release| Manual
=======================

.. toctree::
   :maxdepth: 2
   :numbered:

   manual/introduction
   manual/getting-started
   manual/command-line-options
   manual/input-structure
   manual/parameters
   manual/tips-and-tricks
   manual/a-posteriori-scale-variations
   manual/customization
   manual/examples
   manual/outro

.. toctree::
   :hidden:

   manual/references

* :ref:`genindex`
