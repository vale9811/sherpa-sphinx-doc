#!/usr/bin/env python3

import re
from sys import argv
from bs4 import BeautifulSoup

blacklist = [
    "'PARAMETER: Value'",
    "'Tags: {TAG: Value}'",
    "command line option"
]

brack_re = re.compile('\s*<.*?>\s*')
with open(argv[1], 'r') as index_file:
    soup = BeautifulSoup(index_file.read(), features='lxml')

    symbols = []
    for entry in soup.select('.indextable ul>li>a'):
        symbols.append(entry.get_text())

    # filter blacklist and replace <...>
    items = [re.sub(brack_re, '', sym) for sym in symbols if sym not in blacklist]
    symbols = []
    flags = []

    for sym in items:
        if sym[0] == '-':
            flags.append(sym)
        else:
            symbols.append(sym)

    # write bash completion index
    with open('completion.index', 'w') as bash:
        for sym in symbols:
            bash.write('* ' + sym + '\n')

    with open('options.index', 'w') as bash:
        bash.write(" ".join(flags))
